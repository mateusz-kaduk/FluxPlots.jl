# FluxPlots

[![pipeline status](https://gitlab.com/mateusz-kaduk/FluxPlots.jl/badges/main/pipeline.svg)](https://gitlab.com/mateusz-kaduk/FluxPlots.jl/commits/main)
[![coverage report](https://gitlab.com/mateusz-kaduk/FluxPlots.jl/badges/main/coverage.svg)](https://gitlab.com/mateusz-kaduk/FluxPlots.jl/commits/main)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://mateusz-kaduk.gitlab.io/FluxPlots.jl/dev/)

FluxPlot is a minimal convinience package for plotting training results. It separates plotting library dependencies from the Flux projects.
