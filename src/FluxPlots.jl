module FluxPlots
    using CSV
    using DataFrames
    using AlgebraOfGraphics
    using CairoMakie
    using ImageMagick

    """
    Load CSV and add batch=1:nrows columns as well as model name column.
    """
    function load_csv(path::String; model="default")
        df = CSV.File(path) |> DataFrame
        df[!,:batch] = 1:nrow(df)
        df[!,:model] .= model
        df
    end

    """
    Helper function for loading multiple CSV files.
    """
    function load_dataframe(paths...)
        models = vcat([load_csv(path, model=split(path,'/')[end]) for path in paths]...)
    end

    """
    Plot dataframe grouped by 'col' variable.
    The 'span' parameter is the percent of datapoints used for smoothing with cubic polynomial.
    Lastly 'logerror' is a switch parameter to substract accuracy from one and log it.
    """
    function plot_dataframe(df; col=:train, span=0.05, logerror=false, degree=3)
        df_tmp = copy(df)
        if logerror
            df_tmp[:,col] = log.(1 .- df_tmp[:,col] .+ 0.1e-15)
        end
        plt = data(df_tmp) *
            mapping(:batch, col, color= :model) * (smooth(span=span,degree=degree))
        draw(plt)
    end

    """
    Helper function save plot without loading all the dependecies.
    """
    function save_plot(path, plt)
        save(path, plt)
    end
end # module
