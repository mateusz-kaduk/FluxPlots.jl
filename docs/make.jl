using FluxPlots
using Documenter

DocMeta.setdocmeta!(FluxPlots, :DocTestSetup, :(using FluxPlots); recursive=true)

makedocs(;
    modules=[FluxPlots],
    authors="Mateusz Kaduk <mateusz.kaduk@ki.se> and contributors",
    repo="https://gitlab.com/mateusz-kaduk/FluxPlots.jl/blob/{commit}{path}#{line}",
    sitename="FluxPlots.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mateusz-kaduk.gitlab.io/FluxPlots.jl/dev/",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
